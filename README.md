# Ruth

Server monitoring tool on steroids!

## Docker setup

### Prerequisities

In order to run this container you'll need *docker* & *docker compose* installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Start services with docker compose

```shell
docker-compose up -d
```

Migrate database

```shell
docker-compose run django python manage.py migrate
```

Create django superuser

```shell
docker-compose run django python manage.py createsuperuser
```

Open django index at [localhost:8000](localhost:8000)

## Authors

* **Patrik Romšák** - [@jayor](https://gitlab.com/jayor)
* **Matěj Čermák** - [@cermatej](https://gitlab.com/cermatej)
