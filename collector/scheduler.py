import sched
import time
import collector.startdb as startdb



timing = sched.scheduler(time.time, time.sleep)

def main():
    timing.enter(3,1,startdb.dbcolfun.checkerCol,kwargs={'agent':'MemCol'})
    timing.enter(2,1,startdb.dbcolfun.checkerCol,kwargs={'agent':'CPUCol'})
    timing.enter(10,1,startdb.dbcolfun.checkerCol,kwargs={'agent':'DiskCol'})
    timing.enter(5,2,startdb.dbcolfun.checkerCol,kwargs={'agent':'ProcCol'})
    timing.run()


if __name__ == '__main__':
    while True:
        main()