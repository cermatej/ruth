import json
import time

import psutil

import collector.appender as appender

PROC_INFO_KEYS = ['pid', 'name', 'username', 'terminal', 'cpu_percent', 'memory_percent', 'ppid',
                  'create_time', 'status', 'nice', 'cpu_num', 'cmdline']
RAM_INFO_KEYS = ['total', 'available', 'used', 'used', 'free', 'active']
SWAP_INFO_KEYS = ['total', 'used', 'free']
DISK_INFO_KEYS = ['device', 'mountpoint', 'fstype']
DISK_USAGE_KEYS = ['total', 'used', 'free']

def collector(name):
    if name == "CPUCol":
        colCpu()
    elif name == "MemCol":
        colMem()
    elif name == "ProcCol":
        colPros()
    elif name == "DiskCol":
        colDisk()


def colMem():
    event = []
    timestamp=int(time.time())
    ram_dict = {f'{key}Ram': psutil.virtual_memory()._asdict()[key] for key in RAM_INFO_KEYS}
    swap_dict = {f'{key}Swap': psutil.swap_memory()._asdict()[key] for key in SWAP_INFO_KEYS}
    event.append({**ram_dict, **swap_dict})
    print(json.dumps(event))

    appender.insert('mem', timestamp, event)

def colDisk():
    event = []
    timestamp = int(time.time())
    for disk_info in psutil.disk_partitions():
        disk_info_dict = {key: disk_info._asdict()[key] for key in DISK_INFO_KEYS}
        disk_usage = psutil.disk_usage(disk_info_dict['mountpoint'])
        disk_usage_dict = {key: disk_usage._asdict()[key] for key in DISK_USAGE_KEYS}
        event.append({**disk_info_dict, **disk_usage_dict})
    print(json.dumps(event))

    appender.insert('disk', timestamp, event)

def colPros():
    event = []
    timestamp = int(time.time())
    for proc in psutil.process_iter(attrs=PROC_INFO_KEYS):
        event.append({key: ' '.join(proc.info[key]) if isinstance(proc.info[key], list) else proc.info[key] for key in
                      PROC_INFO_KEYS})
    print(json.dumps(event))

    appender.insert('proc', timestamp, event)

def colCpu():
    event = []
    timestamp=int(time.time())
    event.append({'cpuLoad': (psutil.cpu_percent())})
    print(json.dumps(event))

    appender.insert('cpu', timestamp, event)
