import json
import os
from typing import List

import redis


def insert(perip: str, timestamp: int, data: List[dict]) -> None:
    redis_key: str = f'host:{os.getenv("HOSTID")}:{perip}'
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    r.zadd(redis_key, timestamp, json.dumps(data))
