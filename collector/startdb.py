from sqlalchemy import  Column, Integer, VARCHAR, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import socket
import sched
import time
import collector.colFunc as colFunc

timing = sched.scheduler(time.time, time.sleep)
db_string='postgresql+psycopg2://postgres:postgres@ruth_postgres_1:5432/ruth'

db=create_engine(db_string)
base = declarative_base()

class StatusTable(base):
    __tablename__ = 'StatusTable'
    title= Column('ID',Integer,primary_key=True,autoincrement=True)
    hostname= Column('hostname',VARCHAR(50))
    agent_name= Column('agent_name',VARCHAR(50))
    status= Column('status',VARCHAR(50))

Session = sessionmaker(db)
session = Session()
base.metadata.create_all(db)



host= socket.gethostname()
agent=""
stat="offline"
info=StatusTable(hostname=host,agent_name=agent,status=stat)

class dbcolfun():
    stav = ""

    def dataimport(agent,stat):
        info = StatusTable(hostname=host, agent_name=agent, status=stat)
        print(agent,stat,host)
        session.add(info)
        session.commit()

    def dataread(agent):
        collread=session.query(StatusTable).filter(StatusTable.hostname==host).filter(StatusTable.agent_name==agent)
        session.commit()
        for data in collread:
            dbcolfun.stav=str(data.status)

    def datadelete(agent):
        collread = session.query(StatusTable).filter(StatusTable.hostname==host).filter(StatusTable.agent_name==agent)
        session.commit()
        for data in collread:
            print(data)
            session.delete(data)
            session.commit()

    def dataupdate(agent):
        collread=session.query(StatusTable).filter(StatusTable.hostname == host).filter(StatusTable.agent_name == agent)
        session.commit()
        for data in collread:
            print(data.status)
            data.status="Online"
            session.commit()


    def checkerCol(agent):
        dbcolfun.dataread(agent)
        if dbcolfun.stav == "Offline":
            print("We are offline")
        elif dbcolfun.stav == "Online":
            print("We are online")
            colFunc.collector(name=agent)
        else:
            print("Need to create data for agent:",agent)
            dbcolfun.dataimport(agent,stat="Offline")









#def crRuthDB():
#    engine = create_engine('postgresql+psycopg2://postgres:postgres@localhost:5432')
 #   conn = engine.connect()
  #  conn.execute("commit")
   # conn.execute("create database ruth")
    #conn.close()