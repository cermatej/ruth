from django import forms


class DateRangePickerForm(forms.Form):
    time_range = forms.DateTimeField(widget=forms.TextInput(attrs={'class': 'datepicker'}))
