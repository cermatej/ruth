import json
from typing import List

import redis
from django.db import models

from .utils import DateObj


def parse_json(json_str: str) -> dict:
    return json.loads(json_str.decode('utf8').replace("'", '"'))


class Host(models.Model):
    host_id = models.IntegerField()
    host_name = models.CharField(max_length=200)

    def get_redis_key(self, perip: str) -> str:
        return f'host:{self.host_id}:{perip}'

    # todo move to utils
    def get_redis_instance(self) -> redis.StrictRedis:
        return redis.StrictRedis(host='redis', port=6379, db=0)

    def get_last_perip_state(self, perip: str):
        redis_key = self.get_redis_key(perip)
        r = self.get_redis_instance()
        return [parse_json(disk) for disk in r.zrange(redis_key, -1, -1)]

    def get_perip_events(self, perip: str, time_from: DateObj, time_to: DateObj) -> List[List[dict]]:
        redis_key = self.get_redis_key(perip)
        r = self.get_redis_instance()

        events: List[List[dict]] = []
        for events_json, ts in r.zrangebyscore(redis_key, time_from.timestamp(), time_to.timestamp(), withscores=True):
            e_list: dict = parse_json(events_json)
            # append timestamps
            for e in e_list:
                e['timestamp'] = int(ts)
            events.append(e_list)
        return events

    def __str__(self) -> str:
        return self.host_name
