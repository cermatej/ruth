# Generated by Django 2.1 on 2018-08-21 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='agent_id',
            field=models.IntegerField(default=7),
            preserve_default=False,
        ),
    ]
