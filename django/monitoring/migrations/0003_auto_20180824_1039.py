# Generated by Django 2.1 on 2018-08-24 10:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0002_agent_agent_id'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Agent',
            new_name='Host',
        ),
    ]
