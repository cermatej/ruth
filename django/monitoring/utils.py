from datetime import datetime, timedelta
from typing import List, Dict, Tuple, Union

import bokeh.palettes as bp
import numpy as np
from bokeh.embed import components
from bokeh.models import ColumnDataSource, HoverTool
from bokeh.plotting import figure

BOKEH_GRAPH_GRANULARITY = 300

class DateObj(object):
    DATE_FORMAT = '%Y-%m-%d %I:%M %p'

    def __init__(self, timestamp: Union[int, float, str]):
        self.datetime = datetime.fromtimestamp(int(timestamp))

    @classmethod
    def from_str(cls, date_str: str) -> 'DateObj':
        dt = datetime.strptime(date_str, cls.DATE_FORMAT)
        return DateObj(int(dt.timestamp()))

    def timestamp(self) -> float:
        return self.datetime.timestamp()

    def __str__(self) -> str:
        return datetime.strftime(self.datetime, self.DATE_FORMAT)

    def __gt__(self, other) -> bool:
        return self.datetime > other.datetime


class SizeObj(object):
    UNIT_DEF = {
        'memory': {'B': 0, 'Kb': 3, 'MB': 6, 'GB': 9, 'TB': 12},
        'percent': {'%': 0}
    }
    PRECISION = 2

    def __init__(self, size: int, unit: str):
        self.size: int = size
        self.unit: str = unit

    @classmethod
    def get_memory_size(cls, size: int, unit: str = None) -> 'SizeObj':
        unit = unit if unit else cls.get_memory_unit(size)
        return SizeObj(size, unit)

    @classmethod
    def get_percent_size(cls, size: int) -> 'SizeObj':
        return SizeObj(size, '%')

    @classmethod
    def get_tenths_from_unit(cls, unit_str: str) -> int:
        for _, units in cls.UNIT_DEF.items():
            for unit, tenth in units.items():
                if unit == unit_str:
                    return tenth
        return 0

    @classmethod
    def get_memory_unit(cls, bytes_size: int) -> str:
        for _, units in cls.UNIT_DEF.items():
            for unit, tenths in units.items():
                if len(str(abs(bytes_size))) <= (tenths + 3):
                    return unit
        return ''

    def get_size(self, unit: str) -> float:
        tenths: int = SizeObj.get_tenths_from_unit(unit)
        return self.get_size_tenths(tenths)

    def get_size_tenths(self, tenths: int) -> float:
        return round(self.size / (10 ** tenths), self.PRECISION)

    def get_unit_str(self, unit) -> str:
        return f'{self.get_size(unit)} {unit}'

    def __str__(self) -> str:
        return self.get_unit_str(self.unit)

    def __gt__(self, other) -> bool:
        return self.size > other.size


class BokehGraphs(object):
    FIG_DEF_WIDTH = 1400
    FIG_DEF_HEIGHT = 350
    COLOR_PALETTE = bp.all_palettes['Colorblind'][8]

    @classmethod
    def get_line_graph(
            cls,
            event_data: List[List[dict]],
            value_key: str,
            min_value: int,
            total_def: Union[str, SizeObj],
            dt_from: DateObj,
            dt_to: DateObj,
            unit: str = None,
            title: str = '',
            data_group_key: str = None
    ) -> dict:

        # get total value from highest of the first event
        total_so: SizeObj = max([event[total_def] for event in event_data[0]]) if isinstance(total_def,
                                                                                             str) else total_def
        unit: str = unit if unit else total_so.unit
        title: str = f'{title} - ( total: {total_so.get_unit_str(unit)} )'
        values: Dict[str, List[Tuple[DateObj, SizeObj]]] = cls.__get_graph_values(data_group_key, event_data,
                                                                                  title, value_key, dt_from, dt_to)

        plot: figure = cls.__get_figure(min_value, title, total_so, unit)
        raw_xs, raw_ys = cls.__draw_scatters(plot, unit, values)
        cls.__draw_multiline(plot, raw_xs, raw_ys)

        script, div = components(plot)

        return {'script': script, 'div': div}

    @classmethod
    def __draw_multiline(cls, plot: figure, raw_xs: List[List[float]], raw_ys: List[List[float]]) -> None:
        source = ColumnDataSource(data=dict(
            x=raw_xs,
            y=raw_ys,
            color=cls.COLOR_PALETTE[:len(raw_xs)]
        ))
        plot.multi_line(xs='x', ys='y', line_color='color', line_width=3, source=source)

    @classmethod
    def __draw_scatters(cls, plot: figure, unit: str, values) -> Tuple[List[List[float]], List[List[float]]]:
        raw_xs: List[List[float]] = []
        raw_ys: List[List[float]] = []
        for dg_val, val_list in values.items():
            x = list(range(len(val_list)))
            y = [so.get_size(unit) if so is not None else np.nan for _, so in val_list]
            raw_xs.append(x)
            raw_ys.append(y)

            # draw scatter points for hover
            source = ColumnDataSource({
                'x': x,
                'y': y,
                'val': [str(so) for _, so in val_list],
                'dt': [do.datetime for do, _ in val_list],
                'desc': [dg_val] * len(val_list),
            })
            plot.scatter(x='x', y='y', name='scatter', size=(cls.FIG_DEF_WIDTH - 100) / len(val_list),
                         source=source, fill_alpha=0, line_alpha=0)

            # hover tool for each scatter
            hover = plot.select(dict(type=HoverTool))
            script = '<style>.bk-tooltip>div:not(:first-child) {display:none;}</style>'
            hover.names = ['scatter']
            hover.tooltips = [('name', '@desc'), ('value', '@val'), ('timestamp', '@dt{%d/%m/%Y %X}' + script)]
            hover.formatters = {'dt': 'datetime'}
            hover.mode = 'vline'
        return raw_xs, raw_ys

    @classmethod
    def __get_figure(cls, min_value: int, title: str, total_so: SizeObj, unit: str) -> figure:
        return figure(
            title=title,
            x_axis_type=None,
            y_range=[min_value, total_so.get_size(unit)],
            plot_width=cls.FIG_DEF_WIDTH,
            plot_height=cls.FIG_DEF_HEIGHT,
            tools='hover',
            toolbar_location=None,
            background_fill_color='whitesmoke',
            background_fill_alpha=0.5,
            outline_line_width=2,
            outline_line_color='#3e3e3e'
        )

    @classmethod
    def __get_event_so(cls, single_event: dict, value_key: str) -> SizeObj:
        return single_event[value_key]

    @classmethod
    def __get_event_timestamp(cls, single_event: dict) -> DateObj:
        return single_event['timestamp']

    @classmethod
    def __dt_interval_range(cls, start_date: DateObj, interval_seconds: float):
        # todo yield intervals calculated by whole range in seconds divided by "granularity" constant
        for n in range(1 + BOKEH_GRAPH_GRANULARITY):
            yield start_date.datetime + timedelta(seconds=interval_seconds * n)

    @classmethod
    def ___get_graph_seconds_range(cls, event_list: List[Tuple[DateObj, SizeObj]], start_date: DateObj,
                                   end_date: DateObj) -> int:
        int_seconds = [(event_list[k + 1][0].datetime - event_list[k][0].datetime).total_seconds() for k, event in
                       enumerate(event_list) if k < len(event_list) - 1]
        avg_seconds = sum(int_seconds) // len(int_seconds)
        bokeh_range_seconds = (end_date.datetime - start_date.datetime).total_seconds() // BOKEH_GRAPH_GRANULARITY

        return int(bokeh_range_seconds) if bokeh_range_seconds > avg_seconds else int(avg_seconds)

    @classmethod
    def __get_graph_values(cls, data_group_key: str, event_data: List[List[dict]], title: str, value_key: str,
                           dt_from: DateObj, dt_to: DateObj) -> Dict[
        str, List[Tuple[DateObj, SizeObj]]]:
        values: Dict[str, List[Tuple[DateObj, SizeObj]]] = {}
        if data_group_key:
            # get every possible values from event data
            data_group_vals = set([single_event[data_group_key] for single_event in event_data[0]])
            for group_val in data_group_vals:
                values[group_val]: List[Tuple[DateObj, SizeObj]] = []
                for event_list in event_data:
                    for single_event in event_list:
                        if single_event[data_group_key] == group_val:
                            values[group_val].append((cls.__get_event_timestamp(single_event),
                                                      cls.__get_event_so(single_event, value_key)))
                            break
        else:
            values[title] = [
                (cls.__get_event_timestamp(single_event), cls.__get_event_so(single_event, value_key)) for
                event_list in event_data for single_event in event_list]

        return cls.__generate_intervals(values, dt_from, dt_to)

    @classmethod
    def __generate_intervals(cls, values, dt_from: DateObj, dt_to: DateObj) -> Dict[str, List[Tuple[DateObj, SizeObj]]]:

        new_values: Dict[str, List[Tuple[DateObj, SizeObj]]] = {}
        for title, event_list in values.items():
            new_values[title] = []
            td_interval: timedelta = timedelta(seconds=cls.___get_graph_seconds_range(event_list, dt_from, dt_to))

            dt_gen = cls.__dt_interval_range(dt_from, td_interval.total_seconds())
            dt_event = next(dt_gen)
            val: List[SizeObj] = []
            for do, so in event_list:
                event_used = False
                while not event_used:
                    if cls.__in_interval(do, dt_event, td_interval):  # event matches interval - get next time event
                        val.append(so)
                        event_used = True
                    else:  # event does not match the interval - flush pending and get next interval
                        new_values[title].append(cls.__get_so_avg_tuple(dt_event, val))
                        val = []
                        dt_event = next(dt_gen)
            new_values[title].append(cls.__get_so_avg_tuple(dt_event, val))  # flush last interval
            for dt_event in dt_gen:  # check missing intervals
                new_values[title].append(cls.__get_so_avg_tuple(dt_event, None))
        return new_values

    @classmethod
    def __get_so_avg_tuple(cls, dt_event: datetime, val: List[SizeObj]) -> Tuple[DateObj, SizeObj]:
        vals = [so.size for so in val] if val else None
        fin_val = SizeObj(int(sum(vals) / len(vals)), val[0].unit) if vals else None
        return (DateObj(dt_event.timestamp()), fin_val)

    @classmethod
    def get_hbars(cls):
        # todo generate horizontal bars with maximum value
        pass

    @classmethod
    def __in_interval(cls, what: DateObj, dt_event: datetime, td_interval: timedelta) -> bool:
        date_from: datetime = dt_event - (td_interval / 2)
        date_to: datetime = dt_event + (td_interval / 2)
        return date_from <= what.datetime < date_to


class EventsConverter(object):
    MEMORY_VALUES = ['totalRam', 'freeRam', 'usedRam', 'avalibleRam', 'activeRam', 'totalSwap', 'usedSwap', 'freeSwap',
                     'total', 'used', 'free']
    DATE_VALUES = ['timestamp']
    PERC_VALUES = ['cpuLoad']

    @classmethod
    def convert_values(cls, event_data: List[List[dict]]) -> List[List[dict]]:
        conv: List[List[dict]] = []
        for event_list in event_data:
            m: List[dict] = []
            for single_event in event_list:
                e: dict = {}
                for k, v in single_event.items():
                    if k in cls.MEMORY_VALUES:
                        e[k] = SizeObj.get_memory_size(v)
                    elif k in cls.DATE_VALUES:
                        e[k] = DateObj(single_event[k])
                    elif k in cls.PERC_VALUES:
                        e[k] = SizeObj.get_percent_size(v)
                    else:
                        e[k] = v
                m.append(e)
            conv.append(m)
        return conv
