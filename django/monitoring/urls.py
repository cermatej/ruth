from django.urls import path

from . import views

app_name = 'monitoring'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:host_id>', views.detail, name='detail'),
    path('<int:host_id>/memory/', views.memory_events, name='memory_events'),
    path('<int:host_id>/disk/', views.disk_events, name='disk_events'),
    path('<int:host_id>/cpu/', views.cpu_events, name='cpu_events'),
    path('<int:host_id>/proc/', views.proc_events, name='proc_events'),
]
