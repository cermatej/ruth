from datetime import datetime, timedelta
from typing import List, Tuple

import pandas as pd
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from .forms import DateRangePickerForm
from .models import Host
from .utils import BokehGraphs, EventsConverter, DateObj, SizeObj

DATE_FORMAT = '%Y-%m-%d %I:%M %p'


# HOST PAGES

def index(request) -> HttpResponse:
    hosts: Host = Host.objects.all()
    return render(request, 'monitoring/index.html', {'hosts': hosts})


def detail(request, host_id: int) -> HttpResponse:
    host: Host = get_object_or_404(Host, pk=host_id)
    return render(request, 'monitoring/detail.html', {'host': host})


# MEMORY MONITORING VIEWS

def memory_events(request, host_id: int) -> HttpResponse:
    host: Host = get_object_or_404(Host, pk=host_id)
    perip: str = 'mem'
    events, from_dt, to_dt, templ_data = __get_perip_view_data(host, perip, request)

    if events:
        templ_data['ram_events_graph'] = BokehGraphs.get_line_graph(events, 'usedRam', 0, 'totalRam', from_dt, to_dt,
                                                                    title='Memory usage')
        templ_data['swap_events_graph'] = BokehGraphs.get_line_graph(events, 'usedSwap', 0, 'totalSwap', from_dt, to_dt,
                                                                     title='Swap usage')
    return render(request, 'monitoring/memory.html', templ_data)



# DISK MONITORING EVENTS

def disk_events(request, host_id: int) -> HttpResponse:
    host: Host = get_object_or_404(Host, pk=host_id)
    perip: str = 'disk'
    events, from_dt, to_dt, templ_data = __get_perip_view_data(host, perip, request)

    # last_disks_event: List[dict] = host.get_last_perip_state(perip)
    if events:
        templ_data['disk_events_graph'] = BokehGraphs.get_line_graph(events, 'used', 0, 'total', from_dt, to_dt,
                                                                     data_group_key='device', title='Disk usage')
        # templ_data['disk_current_state'] = BokehGraphs.get_hbars(last_disks_event, 'diskUsed', 'diskSize', title='Current disk state')
    return render(request, 'monitoring/disk.html', templ_data)


# CPU MONITORING EVENTS

def cpu_events(request, host_id: int) -> HttpResponse:
    host: Host = get_object_or_404(Host, pk=host_id)
    perip: str = 'cpu'
    events, from_dt, to_dt, templ_data = __get_perip_view_data(host, perip, request)

    if events:
        templ_data['cpu_events_graph'] = BokehGraphs.get_line_graph(events, 'cpuLoad', 0,
                                                                    SizeObj.get_percent_size(100), from_dt, to_dt,
                                                                    title='CPU usage')
    return render(request, 'monitoring/cpu.html', templ_data)


# PROC MONITORING EVENTS

def proc_events(request, host_id: int) -> HttpResponse:
    host: Host = get_object_or_404(Host, pk=host_id)
    perip: str = 'proc'
    events, from_dt, to_dt, templ_data = __get_perip_view_data(host, perip, request)

    if events:
        df: pd.DataFrame = pd.DataFrame([event for event_list in events for event in event_list])

        df = df.groupby(['pid', 'name', 'cmdline']).agg(
            {'ppid': 'first', 'username': 'first', 'nice': 'first', 'memory_percent': 'mean',
             'cpu_percent': 'mean'}).reset_index()
        df.set_index('pid')

        # todo add nice, create time as dateobj, ppid, username
        templ_data['proc_list'] = df.to_html()

    return render(request, 'monitoring/proc.html', templ_data)

# HELPER METHODS

def __get_range_params(request) -> Tuple[DateObj, DateObj]:
    range_str = request.GET.get('time_range')
    if range_str:
        ranges: List[DateObj] = [DateObj.from_str(date_str) for date_str in range_str.split('  -  ')]
        return ranges[0], ranges[1]
    now_dt = datetime.now().replace(second=0, microsecond=0)

    return DateObj((now_dt - timedelta(hours=1)).timestamp()), DateObj(now_dt.timestamp())


def __get_perip_view_data(host: Host, perip: str, request):
    form = DateRangePickerForm()
    from_dt, to_dt = __get_range_params(request)

    events = host.get_perip_events(perip, from_dt, to_dt)
    events = EventsConverter.convert_values(events) if events else None

    templ_data: dict = {'host': host, 'events': events, 'form': form, 'from_dt': str(from_dt),
                        'to_dt': str(to_dt)}
    return events, from_dt, to_dt, templ_data
